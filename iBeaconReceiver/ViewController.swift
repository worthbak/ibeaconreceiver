//
//  ViewController.swift
//  iBeaconReceiver
//
//  Created by Worth Baker on 2/2/15.
//  Copyright (c) 2015 Worth Baker. All rights reserved.
//

import UIKit
import CoreLocation
import CoreBluetooth

class ViewController: UIViewController, CLLocationManagerDelegate {
    
    var locationManager = CLLocationManager()
    var isInsideRegion = false
    var hasChecked = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.locationManager.delegate = self
        
        if CLLocationManager.authorizationStatus() != CLAuthorizationStatus.AuthorizedWhenInUse {
            self.locationManager.requestWhenInUseAuthorization()
        }
        
        let estimoteUUID = NSUUID(UUIDString: "B9407F30-F5F8-466E-AFF9-25556B57FE6D")!
        
        let region = CLBeaconRegion(proximityUUID: estimoteUUID, identifier: "Estimote Range")!
        
        region.notifyEntryStateOnDisplay = true
        
        if CLLocationManager.isMonitoringAvailableForClass(CLBeaconRegion) {
            locationManager.startRangingBeaconsInRegion(region)
            
            // locationManager.requestStateForRegion(region)
        } else {
            println("This device does not support monitoring beacon regions")
        }
        
        let vc = self
        vc.view.backgroundColor = UIColor.blackColor()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func sendEnterLocalNotification() {
        if !self.isInsideRegion {
            let notice = UILocalNotification()
            
            notice.alertBody = "Inside iBeacon region!"
            notice.alertAction = "Open"
            
            UIApplication.sharedApplication().scheduleLocalNotification(notice)
        }
        
        self.isInsideRegion = true
    }
    
    func sendExitLocalNotification() {
        if self.isInsideRegion {
            let notice = UILocalNotification()
            
            notice.alertBody = "Left iBeacon region!"
            notice.alertAction = "Open"
            
            UIApplication.sharedApplication().scheduleLocalNotification(notice)
        }
        
        self.isInsideRegion = false
    }
    
    func updateUIForState(state: CLRegionState) {
        let vc = self
        
        if state == .Inside {
            vc.view.backgroundColor = UIColor.blueColor()
        } else if state == .Outside {
            vc.view.backgroundColor = UIColor.greenColor()
        } else {
            vc.view.backgroundColor = UIColor.redColor()
        }
    }
    
    func locationManager(manager: CLLocationManager!, didDetermineState state: CLRegionState, forRegion region: CLRegion!) {
        self.updateUIForState(state)
        
        if UIApplication.sharedApplication().applicationState == .Active {
            return
        }
        
        if state == .Inside {
            self.sendEnterLocalNotification()
        } else {
            self.sendExitLocalNotification()
        }
    }
    
    func locationManager(manager: CLLocationManager!, didRangeBeacons beacons: [AnyObject]!, inRegion region: CLBeaconRegion!) {
            // 25063
        
       
        
            for beacon in beacons as [CLBeacon] {
                
                
                
                if beacon.major.description == "25063" && beacon.accuracy < 1.0 {
                    self.view.backgroundColor = UIColor.blueColor()
                    println(beacon)
                   // self.hasChecked = true
                }
                else if beacon.major.description == "25063" && beacon.accuracy >= 1.0 && beacon.accuracy < 2.0
                {
                    println(beacon)
                    self.view.backgroundColor = UIColor.purpleColor()
                   // self.hasChecked = true
                }
                else if (beacon.major.description == "25063")
                {
                    println(beacon)
                    self.view.backgroundColor = UIColor.redColor()
                }
                
                
            
        }
    }
    
    
}

